import pandas as pd
from random import random, randint
import uuid

# PLEASE EXECUTE IN CONSOLE!

# ----------------------------Data____used------------------------------

data_drink0 = pd.read_csv("./Data/proba_drink.csv", sep=",")
data_food0 = pd.read_csv("./Data/proba_food.csv", sep=",")
data_drink = data_drink0.set_index('TIME')
data_food = data_food0.set_index('TIME')  # hours as index to use them in the function commande and easier to get their probabilities in a list

#------------------------------------------------------SUPERCLASS---------------------------------------------------------------
class Customer(object):
    global list_h
    list_h = []

    def __init__(self, budget):
        self.id = str(uuid.uuid4().fields[-1])[:12]
        self.budget = budget

    # Method which select the ordered food and drink among probabilities
    def select(self, hour):
        prob_drink = list(data_drink.loc[hour])  # for the row associated with the hour
        prob_food = list(data_food.loc[hour])
        liste_drink = list(data_drink.columns)  # take the columns
        liste_food = list(data_food.columns)

        rel_pr_d = [weight / 100 for weight in prob_drink]  # to put probabilities in percentage
        rel_pr_f = [weight / 100 for weight in prob_food]

        cum_prob_d = [sum(rel_pr_d[:i + 1]) for i in range(len(rel_pr_d))]  # cumulative proba
        cum_prob_f = [sum(rel_pr_f[:i + 1]) for i in range(len(rel_pr_f))]

        for (i, drink) in enumerate(liste_drink):
            if random() <= cum_prob_d[i]:
                break
        for (i, food) in enumerate(liste_food):
            if random() <= cum_prob_f[i]:
                break
        return drink, food

    def command_and_payment(self, hour):
        food = (self.select(hour)[1])  # to have the return food
        drink = (self.select(hour)[0])
        menu_food = {"sandwich": 5, "cookie": 2, "_nothing": 0, "pie": 3, "muffin": 3}
        menu_drink = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3}

        pricef = menu_food[food]
        priced = menu_drink[drink]
        totalpaid = pricef + priced
        self.budget -= totalpaid

        val_hist = [self.id, food, drink, self.budget]
        key_hist = ["CUSTOMER", "FOOD", "DRINK", "BUDGET"]
        dic_hist = dict(zip(key_hist, val_hist))

        # print("the %s ordered costs %s euros" % (food, pricef))
        # print("the %s ordered costs %s euros" % (drink, priced))
        # print("In total, you have to pay %s euros" % totalpaid)
        # print('your budget is now %s euros' % self.budget)

        return food, pricef, drink, priced, totalpaid, self.budget, dic_hist


#---------------SUBCLASS 1 : ONE TIME CUSTOMER------------------
class Once_Cust(Customer):
    def __init__(self):
        self.id = str(uuid.uuid4().fields[-1])[:12]

#------Subclass 1A : REGULAR--------
class Regular(Once_Cust):
    def __init__(self):
        self.id = str(uuid.uuid4().fields[-1])[:12]
        self.budget = 100

#------Subclass 1B : TRIPADVISOR------
class TripAdvisor(Once_Cust):
    def __init__(self):
        self.id = str(uuid.uuid4().fields[-1])[:12]
        self.budget = 100

    def command_and_payment(self, hour):  #overriding to add the tip
        tip = randint(1, 10)
        food, pricef, drink, priced, totalpaid, self.budget = (Customer.command_and_payment(self, hour)[0:6])
        totalpaid += tip
        self.budget -= tip

        val_hist = [self.id, food, drink, self.budget]
        key_hist = ["CUSTOMER", "FOOD", "DRINK", "BUDGET"]
        dic_hist = dict(zip(key_hist, val_hist))

        return food, pricef, drink, priced, totalpaid, self.budget, dic_hist


#---------------SUBCLASS 2 : RETURNING CUSTOMER------------------
class Returning_Cust(Customer):

    def __init__(self):
        self.id = str(uuid.uuid4().fields[-1])[:12]

    def command_and_payment(self, hour):  #overriding to add the call to historic
        food, pricef, drink, priced, totalpaid, self.budget, dic_hist = (Customer.command_and_payment(self, hour)[0:7])
        self.historic(dic_hist)
        return food, pricef, drink, priced, totalpaid, self.budget, dic_hist

    def historic(self, dic_hist):
        global list_h
        list_h.append(dic_hist)
        return list_h

    def give_histo(self):
        global list_h
        inventory = pd.DataFrame(columns=["BUDGET", "CUSTOMER", "DRINK", "FOOD"])
        for dic in list_h:
            if dic["CUSTOMER"] == self.id:
                inventory = inventory.append(dic, ignore_index=True)
        print(inventory)

#------Subclass 2A : REGULAR-------
class Returning_regular(Returning_Cust):
    def __init__(self):
        self.id = str(uuid.uuid4().fields[-1])[:12]
        self.budget = 250

#------Subclass 2B : HIPSTERS------
class hipsters(Returning_Cust):
    def __init__(self):
        self.id = str(uuid.uuid4().fields[-1])[:12]
        self.budget = 500

