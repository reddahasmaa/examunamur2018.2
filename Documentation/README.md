#CoffeeBar Simulation
The project is to create a simulation of 5 years commands of a Coffeebar.
To do so, we already have a database from which we analyse probabilities.

##Deployment
###To work on the analysis of the given database : Exploratory.py 
###To work on the classes and methods used for the simulation : Creation_customer.py
###To work on the simulation : Simulation2.py
###To work on the analysis of the simulated database : Barplot_Simulated.py
###To work on the effect of changes on the simulated database : Additional_questions.py

##How does it work?
When you call the Simulation2() function it will create 1000 returning customers. Then for every hour of every year it will randomly create
either a returning (20% chance) or a one-time customer and called the command_and_payment() method to simulate a command.
If it creates a returning there is 30% chances it's a hipsters with a 500€ budget and 70% a regular with 250€ budget, 
if it creates a one-time customers, there is 10% chance it's a Tripadvisors and 90% chance it's a regular.
The resulting database will give you the id of the customer and what he ordered for every hour of each of the 5 years

##Authors
Asmaa Reddah
Anne Dewitte
