from random import choice
from Code.Creation_customers import *
import copy


# PLEASE EXECUTE IN CONSOLE!

# let the simulation begin

def Simulation2():
    data = pd.read_csv("./Data/Coffeebar_2013-2017.csv", sep=";")
    time = data["TIME"]

    hip = []
    reg = []
    big_list = []

    # ----Creation of the 1000 returning customers
    for i in range(333):
        hip.append(hipsters())

    for j in range(667):
        reg.append(Returning_regular())
    returning = list(hip + reg)
    returning_full = copy.deepcopy(returning)

    #----Creation of the commands----
    for t in time:
        Type_Cust = ['R', 'R', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O']
        type_Cust = choice(Type_Cust)
        hour = t.split(' ')[1]
        if type_Cust == 'R' and len(returning) != 0:
            ret_cust = choice(returning)
            dic = ret_cust.command_and_payment(hour)[6]  # the dic_hist return in the command_and_payment function
            bud = dic["BUDGET"]
            while bud < 10:
                returning.remove(ret_cust)
                if len(returning) != 0:
                    ret_cust = choice(returning)
                    dic = ret_cust.command_and_payment(hour)[
                        6]  # the dic_hist return in the command_and_payment function
                    bud = dic["BUDGET"]
                else:
                    break
            big_list.append(dic)


        else:
            type_Once = ['T', 'Re', 'Re', 'Re', 'Re', 'Re', 'Re', 'Re', 'Re', 'Re']
            type_once = choice(type_Once)
            if type_once == 'T':
                trip = TripAdvisor()
                dic = trip.command_and_payment(hour)[6]
                big_list.append(dic)

            else:
                regular = Regular()
                dic = regular.command_and_payment(hour)[6]
                big_list.append(dic)

    #----Transformation in DataFrame----
    Coffeebar_sim = pd.DataFrame(big_list)
    Coffeebar_sim = Coffeebar_sim.drop('BUDGET', axis=1)
    Coffeebar_sim = Coffeebar_sim.set_index(time)
    return Coffeebar_sim, returning_full

