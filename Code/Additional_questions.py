import pandas as pd
from random import choice
from Code.Simulation2 import *
import matplotlib as plt
import numpy as np

# PLEASE EXECUTE IN CONSOLE!
# ----------------------------Show some buying histories of returning customers for your simulations--------------------------------
# ATTENTION THIS PART TAKES 5 MINUTES BUT WORKS
data_sim, retur = Simulation2()[0:2]
returning1 = choice(retur)
print(returning1.give_histo())
returning2 = choice(retur)
print(returning2.give_histo())

# ------------------------------------------------------------2----------------------------------------------------------------------

data = pd.read_csv("./Data/Coffeebar_2013-2017.csv", sep=";")
data2 = data.replace(np.nan,
                     '_nothing')
ret = data2.groupby("CUSTOMER").filter(lambda x: len(x) > 1)
num_ret = list(np.unique(ret.CUSTOMER))
print(len(num_ret))
# there is 1000 returning customers

times = pd.to_datetime(data["TIME"]).dt.time
num_time_r = ((ret.TIME).groupby(times)).count()

prob_r = (((ret.TIME).groupby(times)).count()) / 1825  # nbr years * nbr days/year = 5*365
prob_o = pd.Series(1 - prob_r)

# Do you see correlations between what returning customers buy and one-timers?
once = data2.groupby("CUSTOMER").filter(lambda x: len(x) == 1)
count_ret_f = pd.DataFrame(ret.FOOD.groupby(ret.FOOD).count())
count_once_f = pd.DataFrame(once.FOOD.groupby(once.FOOD).count())
corr_food = count_ret_f.corrwith(count_once_f)
print(corr_food)  # very positively correlate FOOD    0.903162

count_ret_d = pd.DataFrame(ret.DRINKS.groupby(ret.DRINKS).count())
count_once_d = pd.DataFrame(once.DRINKS.groupby(once.DRINKS).count())
corr_drink = count_ret_d.corrwith(count_once_d)
print(corr_drink)  # not very correlate DRINKS    0.217604

# ---------------------3.What would happen if we lower the returning customers to 50 and simulate the same period?--------------------
data3 = pd.read_csv("./Data/Coffeebar_sim_q3.csv", sep=",")

# --------------------4.The prices change from the beginning of 2015 and go up by 20%-------------------------------------------------
data4 = pd.read_csv("./Data/Coffeebar_sim_q4.csv", sep=";")

# -------------------------------------------5.The budget of hipsters drops to 40------------------------------------------------------
data5 = pd.read_csv("./Data/Coffeebar_sim_q5.csv", sep=",")

# -------------------------------------------6.There is no returning customer ------------------------------------------------------
data6 = pd.read_csv("./Data/Coffeebar_sim_q6.csv", sep=",")

num_food = data6["FOOD"].groupby(data6['FOOD']).count()
liste_axe = ['nothing', 'cookie', 'muffin', 'pie', 'sandwich']
y_pos = np.arange(len(liste_axe))
# Create bars
plt.bar(y_pos, num_food)
# Create names on the x-axis
plt.xticks(y_pos, liste_axe)
food = plt.figure()

# --------------Bar plot2: type of drinks sold in five years----------------
num_drink = list(data6["DRINK"].groupby(data6["DRINK"]).count())
liste_axis = ['coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water']
y_pos = np.arange(len(liste_axis))
# Create bars
plt.bar(y_pos, num_drink)
# Create names on the x-axis
plt.xticks(y_pos, liste_axis)
drink = plt.figure()
food.show()
drink.show()
