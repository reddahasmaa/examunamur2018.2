from Code.Simulation2 import *
import matplotlib.pyplot as plt
import numpy as np

# PLEASE EXECUTE IN CONSOLE!

# ATTENTION THE SIMULATION PART TAKES 4 MINUTES 30 BUT YOU CAN SEE THE PLOTS IN OUR PAPERS
Sim_data = Simulation2()[0]

#--------------Bar plot1: type of food sold in five years----------------
num_food= Sim_data["FOOD"].groupby(Sim_data['FOOD']).count()
liste_axe = ['nothing','cookie', 'muffin', 'pie', 'sandwich']
nb_bar = np.arange(len(liste_axe))
# Create bars
plt.bar(nb_bar, num_food)
# Create names on the x-axis
plt.xticks(nb_bar, liste_axe)
food= plt.figure()

#--------------Bar plot2: type of drinks sold in five years----------------
num_drink = list(Sim_data["DRINK"].groupby(Sim_data["DRINK"]).count())
liste_axis = ['coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water']
nb_bar = np.arange(len(liste_axis))
# Create bars
plt.bar(nb_bar, num_drink)
# Create names on the x-axis
plt.xticks(nb_bar, liste_axis)
drink= plt.figure()
food.show()
drink.show()
