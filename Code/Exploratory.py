import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# PLEASE EXECUTE IN CONSOLE!
path = "./Data/"
data = pd.read_csv(path + "Coffeebar_2013-2017.csv", sep=";")

# --------------------------------------------Question 1 --------------------------------------------------------
# what food
list_food = []
for food in data['FOOD']:
    if food not in list_food and pd.isnull(food) == False:
        list_food.append(food)
print("The foods sold by the coffebar are : %s" % list_food)

# what drinks
list_drink = []
for drink in data['DRINKS']:
    if drink not in list_drink:
        list_drink.append(drink)
print("The drinks sold by the coffebar are : %s" % list_drink)

# how many customer
list_id = []
for customer in data['CUSTOMER']:
    list_id.append(customer)
print('there are %s customers' % len(set(list_id)))  # set to remove doublon

# --------------------------------------------Question 2 ---------------------------------------------------------


# ------------PLOT 1: total drinks sold per year -------------
num_d = []
years = pd.to_datetime(data["TIME"]).dt.year
year = list(np.unique(years))
print(year)
num_d = data["DRINKS"].groupby(years).count()
nbdrinks = list(num_d)
y_pos = np.arange(len(year))
# Create bars
plt.bar(y_pos, nbdrinks)
# Create names on the x-axis
plt.xticks(y_pos, year)
plt.ylabel("total drink sold")
totD = plt.figure()

# ------------PLOT 2 : total food sold per year -------------
num_f = []
num_f = data["FOOD"].groupby(years).count()
nbfood = list(num_f)
y_pos = np.arange(len(year))
# Create bars
plt.bar(y_pos, nbfood)
# Create names on the x-axis
plt.xticks(y_pos, year)
plt.ylabel("total food sold")
totF = plt.figure()

# ------------PLOT 3 : amount by type of food -------------
num_food = list(data["FOOD"].groupby(data['FOOD']).count())
liste_axe = ['cookie', 'muffin', 'pie', 'sandwich']
y_pos = np.arange(len(liste_axe))
# Create bars
plt.bar(y_pos, num_food)
# Create names on the x-axis
plt.xticks(y_pos, liste_axe)
food = plt.figure()

# ------------PLOT 4 : amount by type of drink-------------
num_drink = data["DRINKS"].groupby(data["DRINKS"]).count()
liste_axis = ['coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water']
y_pos = np.arange(len(liste_axis))
# Create bars
plt.bar(y_pos, num_drink)
# Create names on the x-axis
plt.xticks(y_pos, liste_axis)
drink = plt.figure()

totF.show()
totD.show()
food.show()
drink.show()

# --------------------------------------------Question 3 --------------------------------------------------------
# probabilities
data2 = data.replace(np.nan,
                     '_nothing')  # not in the beginning of the file or it would consider it as a value for the plots

# drinks
times = pd.to_datetime(data2["TIME"]).dt.time
Pr_dr = data2.groupby([times, 'DRINKS']).size().unstack()
proba_drinks = Pr_dr.div(Pr_dr.sum(1), axis=0) * 100
print(proba_drinks)
proba_drinks = pd.DataFrame(proba_drinks)
proba_drink = proba_drinks.to_csv('proba_drink.csv', encoding='utf-8')

# foods
Pr_f = data2.groupby([times, "FOOD"]).size().unstack()
proba_foods = Pr_f.div(Pr_f.sum(1), axis=0) * 100
print(proba_foods)
proba_foods = pd.DataFrame(proba_foods)
proba_food = proba_foods.to_csv('proba_food.csv', encoding='utf-8')  # put in a csv file to read it in Creation Customer
